import 'package:bloc_test/bloc_test.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:geofence_flutter/blocs/connectivity/cubit/connectivity_cubit.dart';
import 'package:mocktail/mocktail.dart';
import 'package:network_info_plus/network_info_plus.dart';

enum RequestStatus {
  SUCCESS,
  FAILED,
}

class MockConnectivity extends Fake implements Connectivity {
  RequestStatus status = RequestStatus.SUCCESS;
  ConnectivityResult successResult = ConnectivityResult.wifi;
  final Stream<ConnectivityResult> _onConnectivityChanged;

  MockConnectivity(this._onConnectivityChanged);

  @override
  Future<ConnectivityResult> checkConnectivity() {
    if (status == RequestStatus.SUCCESS) {
      return Future.value(successResult);
    } else {
      throw Error();
    }
  }

  @override
  Stream<ConnectivityResult> get onConnectivityChanged {
    return _onConnectivityChanged;
  }
}

class MockNetworkInfo extends Fake implements NetworkInfo {
  final LocationAuthorizationStatus authorizeStatus;
  final String? _wifiName;

  MockNetworkInfo(this.authorizeStatus, this._wifiName);

  @override
  Future<LocationAuthorizationStatus> getLocationServiceAuthorization() {
    return Future.value(authorizeStatus);
  }

  @override
  Future<String?> getWifiName() {
    return Future.value(_wifiName);
  }

  @override
  Future<LocationAuthorizationStatus> requestLocationServiceAuthorization(
      {bool requestAlwaysLocationUsage = false}) {
    return Future.value(authorizeStatus);
  }
}

class MockConnectivityCubit extends MockCubit<ConnectivityState>
    implements ConnectivityCubit {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  setUpAll(() {
    registerFallbackValue<ConnectivityState>(ConnectivityState.initial());
  });
  group('Connectivity Cubit-', () {
    final Connectivity mockConnectivity;
    final NetworkInfo mockNetworkInfo;
    final Stream<ConnectivityResult> onConnectivityChanged =
        Stream<ConnectivityResult>.fromFutures([
      Future.value(ConnectivityResult.wifi),
      Future.value(ConnectivityResult.none),
      Future.value(ConnectivityResult.mobile)
    ]);
    mockConnectivity = MockConnectivity(onConnectivityChanged);
    mockNetworkInfo = MockNetworkInfo(
        LocationAuthorizationStatus.authorizedWhenInUse, "WIFI_5gz");

    test("Emit disconnected, connected mobile", () {
      // Create Mock CounterCubit Instance
      final cubit = MockConnectivityCubit();

      // Stub the listen with a fake Stream
      whenListen(
          cubit,
          Stream.fromIterable([
            ConnectivityState.disconnected(null),
            ConnectivityState.connectedMobile(),
          ]));

      expectLater(
          cubit.stream,
          emitsInOrder(<ConnectivityState>[
            ConnectivityState.disconnected(null),
            ConnectivityState.connectedMobile(),
          ]));
    });

    blocTest(
      'emits Connected Wifi when init',
      build: () => ConnectivityCubit(
          connectivity: mockConnectivity, networkInfo: mockNetworkInfo),
      act: (ConnectivityCubit bloc) {
        bloc.initConnectivity();
      },
      expect: () => [
        ConnectivityState.connectedWifi("WIFI_5gz"),
        ConnectivityState.disconnected(null),
        ConnectivityState.connectedWifi("WIFI_5gz"),
        ConnectivityState.connectedMobile(),
      ],
      tearDown: () {},
    );
  });
}
