import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:geofence_flutter/blocs/current_location/cubit/current_location_cubit.dart';
import 'package:location/location.dart';
import 'package:mockito/mockito.dart';

class MockCurrentLocation extends Fake implements LocationData {
  // final

  @override
  var latitude = 3.079265;

  @override
  var longitude = 101.703680;

  // @override
  // LocationData._(
  //     this.latitude,
  //     this.longitude,
  //     this.accuracy,
  //     this.altitude,
  //     this.speed,
  //     this.speedAccuracy,
  //     this.heading,
  //     this.time,
  //     this.isMock,
  //     this.verticalAccuracy,
  //     this.headingAccuracy,
  //     this.elapsedRealtimeNanos,
  //     this.elapsedRealtimeUncertaintyNanos,
  //     this.satelliteNumber,
  //     this.provider);
}

class MockLocationService extends Fake implements Location {
  PermissionStatus permission = PermissionStatus.denied;
  bool serviceIsEnabled = false;

  @override
  Future<LocationData> getLocation() async {
    return MockCurrentLocation();
  }

  @override
  Future<PermissionStatus> hasPermission() async {
    return this.permission = permission;
  }

  @override
  Future<bool> serviceEnabled() async {
    return this.serviceIsEnabled;
  }

  Future<PermissionStatus> requestPermission() async {
    return this.permission;
  }
}

// @GenerateMocks([Location])
void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  final MockLocationService mockService;

  mockService = MockLocationService();

  blocTest(
      "Expect fetch success with correct location data under location and permission enabled status",
      setUp: () {
        mockService.serviceIsEnabled = true;
        mockService.permission = PermissionStatus.granted;
      },
      build: () => CurrentLocationCubit(geolocator: mockService),
      act: (CurrentLocationCubit bloc) => bloc.getCurrentPosition(),
      expect: () => [
            CurrentLocationState.loading(),
            CurrentLocationState.fetchSuccess(3.079265, 101.70368),
          ],
      tearDown: () {});

  blocTest("Expect fetchFailed when LocationPermission status is denied",
      setUp: () {
        mockService.serviceIsEnabled = true;
        mockService.permission = PermissionStatus.denied;
      },
      build: () => CurrentLocationCubit(geolocator: mockService),
      act: (CurrentLocationCubit bloc) => bloc.getCurrentPosition(),
      expect: () => [
            CurrentLocationState.loading(),
            CurrentLocationState.fetchFailed("Location permissions are denied"),
          ],
      tearDown: () {
        mockService.permission = PermissionStatus.denied;
      });

  blocTest("Expect fetchFailed when locationService disabled",
      setUp: () {
        mockService.serviceIsEnabled = false;
      },
      build: () => CurrentLocationCubit(geolocator: mockService),
      act: (CurrentLocationCubit bloc) => bloc.getCurrentPosition(),
      expect: () => [
            CurrentLocationState.loading(),
            CurrentLocationState.fetchFailed("Location services are disabled."),
          ],
      tearDown: () {});
}
