import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geofence_flutter/blocs/geomap/cubit/geomap_cubit.dart';
import 'package:geofence_flutter/blocs/location_permission/cubit/location_permission_cubit.dart';
import 'package:geofence_flutter/screens/home.dart';
import 'package:location/location.dart';

import 'package:network_info_plus/network_info_plus.dart';

import 'bloc_observer.dart';
import 'blocs/connectivity/cubit/connectivity_cubit.dart';
import 'blocs/current_location/cubit/current_location_cubit.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final connectivityCubit = ConnectivityCubit(
      connectivity: new Connectivity(),
      networkInfo: new NetworkInfo(),
    );
    final currentLocationCubit = CurrentLocationCubit(geolocator: Location());

    return MultiBlocProvider(
      providers: [
        BlocProvider<ConnectivityCubit>(
          create: (context) => connectivityCubit,
        ),
        BlocProvider<CurrentLocationCubit>(
          create: (context) => currentLocationCubit,
        ),
        BlocProvider<GeomapCubit>(
          create: (context) => GeomapCubit(currentLocationCubit),
        ),
        BlocProvider<LocationPermissionCubit>(
          create: (context) => LocationPermissionCubit(NetworkInfo()),
        ),
      ],
      child: MaterialApp(
        title: 'Geofence',
        home: HomeScreen(),
      ),
    );
  }
}
