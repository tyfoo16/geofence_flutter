// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'geofence.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GeofenceTearOff {
  const _$GeofenceTearOff();

  _Geofence call(
      {double lat = 0.0,
      double lng = 0.0,
      double radius = 0.0,
      String? wifiName}) {
    return _Geofence(
      lat: lat,
      lng: lng,
      radius: radius,
      wifiName: wifiName,
    );
  }
}

/// @nodoc
const $Geofence = _$GeofenceTearOff();

/// @nodoc
mixin _$Geofence {
  double get lat => throw _privateConstructorUsedError;
  double get lng => throw _privateConstructorUsedError;
  double get radius => throw _privateConstructorUsedError;
  String? get wifiName => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GeofenceCopyWith<Geofence> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GeofenceCopyWith<$Res> {
  factory $GeofenceCopyWith(Geofence value, $Res Function(Geofence) then) =
      _$GeofenceCopyWithImpl<$Res>;
  $Res call({double lat, double lng, double radius, String? wifiName});
}

/// @nodoc
class _$GeofenceCopyWithImpl<$Res> implements $GeofenceCopyWith<$Res> {
  _$GeofenceCopyWithImpl(this._value, this._then);

  final Geofence _value;
  // ignore: unused_field
  final $Res Function(Geofence) _then;

  @override
  $Res call({
    Object? lat = freezed,
    Object? lng = freezed,
    Object? radius = freezed,
    Object? wifiName = freezed,
  }) {
    return _then(_value.copyWith(
      lat: lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double,
      lng: lng == freezed
          ? _value.lng
          : lng // ignore: cast_nullable_to_non_nullable
              as double,
      radius: radius == freezed
          ? _value.radius
          : radius // ignore: cast_nullable_to_non_nullable
              as double,
      wifiName: wifiName == freezed
          ? _value.wifiName
          : wifiName // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$GeofenceCopyWith<$Res> implements $GeofenceCopyWith<$Res> {
  factory _$GeofenceCopyWith(_Geofence value, $Res Function(_Geofence) then) =
      __$GeofenceCopyWithImpl<$Res>;
  @override
  $Res call({double lat, double lng, double radius, String? wifiName});
}

/// @nodoc
class __$GeofenceCopyWithImpl<$Res> extends _$GeofenceCopyWithImpl<$Res>
    implements _$GeofenceCopyWith<$Res> {
  __$GeofenceCopyWithImpl(_Geofence _value, $Res Function(_Geofence) _then)
      : super(_value, (v) => _then(v as _Geofence));

  @override
  _Geofence get _value => super._value as _Geofence;

  @override
  $Res call({
    Object? lat = freezed,
    Object? lng = freezed,
    Object? radius = freezed,
    Object? wifiName = freezed,
  }) {
    return _then(_Geofence(
      lat: lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double,
      lng: lng == freezed
          ? _value.lng
          : lng // ignore: cast_nullable_to_non_nullable
              as double,
      radius: radius == freezed
          ? _value.radius
          : radius // ignore: cast_nullable_to_non_nullable
              as double,
      wifiName: wifiName == freezed
          ? _value.wifiName
          : wifiName // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_Geofence implements _Geofence {
  _$_Geofence(
      {this.lat = 0.0, this.lng = 0.0, this.radius = 0.0, this.wifiName})
      : assert(
            lat >= -90.0 && lat <= 90.0, 'lat should be range from -90 to 90'),
        assert(lng >= -180.0 && lng <= 180.0,
            'lng should be range from -180 to 180');

  @JsonKey(defaultValue: 0.0)
  @override
  final double lat;
  @JsonKey(defaultValue: 0.0)
  @override
  final double lng;
  @JsonKey(defaultValue: 0.0)
  @override
  final double radius;
  @override
  final String? wifiName;

  @override
  String toString() {
    return 'Geofence(lat: $lat, lng: $lng, radius: $radius, wifiName: $wifiName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Geofence &&
            (identical(other.lat, lat) ||
                const DeepCollectionEquality().equals(other.lat, lat)) &&
            (identical(other.lng, lng) ||
                const DeepCollectionEquality().equals(other.lng, lng)) &&
            (identical(other.radius, radius) ||
                const DeepCollectionEquality().equals(other.radius, radius)) &&
            (identical(other.wifiName, wifiName) ||
                const DeepCollectionEquality()
                    .equals(other.wifiName, wifiName)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(lat) ^
      const DeepCollectionEquality().hash(lng) ^
      const DeepCollectionEquality().hash(radius) ^
      const DeepCollectionEquality().hash(wifiName);

  @JsonKey(ignore: true)
  @override
  _$GeofenceCopyWith<_Geofence> get copyWith =>
      __$GeofenceCopyWithImpl<_Geofence>(this, _$identity);
}

abstract class _Geofence implements Geofence {
  factory _Geofence({double lat, double lng, double radius, String? wifiName}) =
      _$_Geofence;

  @override
  double get lat => throw _privateConstructorUsedError;
  @override
  double get lng => throw _privateConstructorUsedError;
  @override
  double get radius => throw _privateConstructorUsedError;
  @override
  String? get wifiName => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$GeofenceCopyWith<_Geofence> get copyWith =>
      throw _privateConstructorUsedError;
}
