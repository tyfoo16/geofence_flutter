import 'package:freezed_annotation/freezed_annotation.dart';
part 'geofence.freezed.dart';

@freezed
class Geofence with _$Geofence {
  @Assert('lat >= -90.0 && lat <= 90.0', 'lat should be range from -90 to 90')
  @Assert(
      'lng >= -180.0 && lng <= 180.0', 'lng should be range from -180 to 180')
  factory Geofence({
    @Default(0.0) double lat,
    @Default(0.0) double lng,
    @Default(0.0) double radius,
    String? wifiName,
  }) = _Geofence;
}
