import 'dart:io';

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geofence_flutter/blocs/connectivity/cubit/connectivity_cubit.dart';
import 'package:geofence_flutter/blocs/current_location/cubit/current_location_cubit.dart';
import 'package:geofence_flutter/blocs/geomap/cubit/geomap_cubit.dart';
import 'package:geofence_flutter/blocs/location_permission/cubit/location_permission_cubit.dart';

import 'package:geofence_flutter/screens/geo_map.dart';
import 'package:network_info_plus/network_info_plus.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late ConnectivityCubit connectivityCubit;

  var wifiName = "";

  @override
  void initState() {
    connectivityCubit = BlocProvider.of<ConnectivityCubit>(context);

    super.initState();
    connectivityCubit.initConnectivity();
    BlocProvider.of<LocationPermissionCubit>(context).requestPermission();
  }

  @override
  void dispose() {
    connectivityCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(),
      body: SafeArea(
        child: Stack(
          children: [
            Positioned.fill(child: _buildMap()),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: _buildConnectivityBar(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildConnectivityBar() {
    return BlocConsumer<ConnectivityCubit, ConnectivityState>(
      listener: (context, state) {},
      builder: (context, state) {
        String networkType = "";
        String? error;
        state.when(
            initial: () {},
            connectedMobile: () {
              networkType = "Mobile Data";
            },
            connectedWifi: (wifiName) {
              networkType = "Wifi";
              this.wifiName = wifiName;
            },
            disconnected: (err) {
              networkType = "Not connected";
              error = err;
            });
        return Container(
          margin: EdgeInsets.all(8),
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Column(
            children: [
              _buildRow("Network Type: ", networkType,
                  isPositive:
                      networkType == "Wifi" || networkType == "Mobile Data"),
              networkType == "Mobile Data"
                  ? SizedBox()
                  : _buildRow("Wifi Name: ",
                      this.wifiName.isEmpty ? "Unknown" : this.wifiName,
                      isPositive: this.wifiName.isNotEmpty),
              _buildRow("", error ?? ""),
              _buildPermissionStatus(),
              _buildStatus(),
            ],
          ),
        );
      },
    );
  }

  Widget _buildPermissionStatus() {
    return BlocConsumer<LocationPermissionCubit, LocationPermissionState>(
      listener: (context, state) {
        state.when(
            initial: () {},
            permissionGranted: () {
              BlocProvider.of<ConnectivityCubit>(context).initConnectivity();
              BlocProvider.of<CurrentLocationCubit>(context)
                  .getCurrentPosition();
            },
            permissionDenied: () {});
      },
      builder: (context, state) {
        return BlocBuilder<LocationPermissionCubit, LocationPermissionState>(
            builder: (context, state) {
          return state.when(initial: () {
            return Container();
          }, permissionGranted: () {
            return Container(
              child: _buildRow("Location Permission: ", "Granted",
                  isPositive: true),
            );
          }, permissionDenied: () {
            return Container(
              child: Column(
                children: [
                  _buildRow("Location Permission", "Denied"),
                  ElevatedButton(
                    child: Text("Get Location Permission"),
                    onPressed: () {
                      BlocProvider.of<LocationPermissionCubit>(context)
                          .requestPermission();
                    },
                  ),
                ],
              ),
            );
          });
        });
      },
    );
  }

  Widget _buildStatus() {
    return BlocBuilder<GeomapCubit, GeomapState>(
      builder: (context, state) {
        return state.when(initial: () {
          return Container();
        }, targetUpdated: (geo, withinRadius) {
          final wifiMatched = geo.wifiName == this.wifiName;
          final status = wifiMatched || withinRadius ? "Inside" : "Outside";
          return Column(
            children: [
              _buildRow(
                "Wifi Name Matched: ",
                wifiMatched.toString(),
                isPositive: wifiMatched,
              ),
              _buildRow(
                "Within radius: ",
                withinRadius.toString(),
                isPositive: withinRadius,
              ),
              _buildRow("Status: ", status, isPositive: status == "Inside"),
            ],
          );
        }, currentLocationUpdated: (double lat, double lng) {
          return Container();
        }, networkUpdated: (NetworkInfo info, bool permissionGranted) {
          return Container();
        });
      },
    );
  }

  Widget _buildRow(String title, String? text, {bool isPositive = false}) {
    return Container(
      padding: EdgeInsets.all(2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title),
          Expanded(
            child: Text(
              text ?? "None",
              maxLines: 2,
              overflow: TextOverflow.fade,
              style: TextStyle(
                color: isPositive ? Colors.green : Colors.red,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMap() {
    return Container(
      color: Colors.lightBlue,
      child: GeoMapScreen(),
    );
  }
}
