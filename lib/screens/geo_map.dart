import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:geofence_flutter/blocs/current_location/cubit/current_location_cubit.dart';
import 'package:geofence_flutter/blocs/geomap/cubit/geomap_cubit.dart';
import 'package:geofence_flutter/models/geofence.dart';
import "package:latlong2/latlong.dart" as latLng;
import 'package:network_info_plus/network_info_plus.dart';

import '../config.dart' as Config;

class GeoMapScreen extends StatefulWidget {
  const GeoMapScreen({Key? key}) : super(key: key);

  @override
  _GeoMapScreenState createState() => _GeoMapScreenState();
}

class _GeoMapScreenState extends State<GeoMapScreen> {
  MapController mapController = MapController();
  final initialTargetLocation =
      latLng.LatLng(Config.initialLat, Config.initialLng);
  final initialWifiName = Config.initialWifiName;
  final initialRadius = Config.initialRadius;

  var zoomLevel = 18.0;
  var radius = 100.0;
  final _formKey = GlobalKey<FormState>();
  double maxSheetHeightRatio = 0.5;
  double? distance;

  late TextEditingController wifiNameCtrl;
  late TextEditingController latCtrl;
  late TextEditingController lngCtrl;
  late TextEditingController radiusCtrl;

  late Geofence targetGeofence;
  latLng.LatLng? currentLocation;

  @override
  void initState() {
    super.initState();
    targetGeofence = Geofence(
      lat: initialTargetLocation.latitude,
      lng: initialTargetLocation.longitude,
      radius: initialRadius,
      wifiName: initialWifiName,
    );
    wifiNameCtrl = TextEditingController(text: targetGeofence.wifiName);
    latCtrl = TextEditingController(text: targetGeofence.lat.toString());
    lngCtrl = TextEditingController(text: targetGeofence.lng.toString());
    radiusCtrl = TextEditingController(text: targetGeofence.radius.toString());
    BlocProvider.of<CurrentLocationCubit>(context).getCurrentPosition();
    BlocProvider.of<GeomapCubit>(context)
        .updateTargetGeofence(targetGeofence, currentLocation: currentLocation);
  }

  @override
  void dispose() {
    wifiNameCtrl.dispose();
    latCtrl.dispose();
    lngCtrl.dispose();
    radiusCtrl.dispose();
    super.dispose();
  }

  // zoomBetweenTargetAndCurrentLocation() {
  // LatLngBounds bound;
  // final targetPos = latLng.LatLng(targetGeofence.lat, targetGeofence.lng);
  // if (targetPos.latitude > currentLocation.latitude &&
  //     targetPos.longitude > currentLocation.longitude) {
  //   bound = LatLngBounds(currentLocation, targetPos);
  // } else if (targetPos.longitude > currentLocation.longitude) {
  //   bound = LatLngBounds(
  //       latLng.LatLng(targetPos.latitude, currentLocation.longitude),
  //       latLng.LatLng(currentLocation.latitude, targetPos.longitude));
  // } else if (targetPos.latitude > currentLocation.latitude) {
  //   bound = LatLngBounds(
  //       latLng.LatLng(currentLocation.latitude, targetPos.longitude),
  //       latLng.LatLng(targetPos.latitude, currentLocation.longitude));
  // } else {
  //   bound = LatLngBounds(targetPos, currentLocation);
  // }

  // mapController.fitBounds(
  //   bound,
  //   options: FitBoundsOptions(
  //     padding: EdgeInsets.all(10.0),
  //     // maxZoom: 22,
  //     // zoom: 20,
  //   ),
  // );
  // }

  @override
  Widget build(BuildContext context) {
    return BlocListener<CurrentLocationCubit, CurrentLocationState>(
      listener: (context, state) {
        state.when(
            initial: () {},
            loading: () {},
            fetchSuccess: (lat, lng) {
              currentLocation = latLng.LatLng(lat, lng);
              BlocProvider.of<GeomapCubit>(context).updateTargetGeofence(
                  targetGeofence,
                  currentLocation: currentLocation);
              mapController.move(latLng.LatLng(lat, lng), zoomLevel);
            },
            fetchFailed: (err) {});
      },
      child: Container(
        child: Stack(
          children: [
            FlutterMap(
              mapController: mapController,
              options: MapOptions(
                center: latLng.LatLng(targetGeofence.lat, targetGeofence.lng),
                zoom: zoomLevel,
              ),
              children: [
                TileLayerWidget(
                    options: TileLayerOptions(
                        urlTemplate:
                            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                        subdomains: ['a', 'b', 'c'])),
                BlocBuilder<GeomapCubit, GeomapState>(
                  builder: (context, state) {
                    Geofence _geo = Geofence();
                    state.when(
                        initial: () {},
                        targetUpdated: (geo, inRadius) {
                          _geo = geo;
                        },
                        currentLocationUpdated: (double lat, double lng) {},
                        networkUpdated:
                            (NetworkInfo info, bool permissionGranted) {});
                    return CircleLayerWidget(
                      options: CircleLayerOptions(
                        circles: [
                          CircleMarker(
                            point: latLng.LatLng(_geo.lat, _geo.lng),
                            color: Colors.blue.withOpacity(0.5),
                            borderStrokeWidth: 3.0,
                            borderColor: Colors.blue,
                            radius: targetGeofence.radius,
                            useRadiusInMeter: true,
                          )
                        ],
                      ),
                    );
                  },
                ),
                BlocBuilder<CurrentLocationCubit, CurrentLocationState>(
                  builder: (context, state) {
                    var _color = Colors.green;
                    var _curPos = latLng.LatLng(0.0, 0.0);
                    state.when(
                        initial: () {},
                        loading: () {
                          _color = Colors.brown;
                        },
                        fetchSuccess: (lat, lng) {
                          _curPos.latitude = lat;
                          _curPos.longitude = lng;
                        },
                        fetchFailed: (err) {});
                    return MarkerLayerWidget(
                        options: MarkerLayerOptions(
                      markers: [
                        Marker(
                          width: 30.0,
                          height: 30.0,
                          point: _curPos,
                          builder: (context) => Container(
                            child: Icon(
                              Icons.location_on,
                              size: 30,
                              color: _color,
                            ),
                          ),
                        ),
                        Marker(
                          width: 30.0,
                          height: 30.0,
                          point: latLng.LatLng(
                              targetGeofence.lat, targetGeofence.lng),
                          builder: (context) => Container(
                            child: Icon(
                              Icons.location_on,
                              size: 30,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ],
                    ));
                  },
                ),
              ],
              layers: [],
            ),
            Positioned(
              bottom: 150,
              right: 12,
              child: _buildCurrentPositionButton(),
            ),
            _buildBottomSheet(),
          ],
        ),
      ),
    );
  }

  Widget _buildPinDrop({bool enabled = true}) {
    return ElevatedButton(
      onPressed: enabled
          ? () async {
              BlocProvider.of<CurrentLocationCubit>(context)
                  .getCurrentPosition();
            }
          : null,
      child: Icon(Icons.location_on, color: Colors.white),
      style: ElevatedButton.styleFrom(
        shape: CircleBorder(),
        primary: Colors.red, // <-- Button color
        onPrimary: Colors.black12, // <-- Splash color
      ),
    );
  }

  Widget _buildCurrentPositionButton() {
    return BlocBuilder<CurrentLocationCubit, CurrentLocationState>(
      builder: (context, state) {
        return state.when(initial: () {
          return _buildPinDrop();
        }, loading: () {
          return _buildPinDrop(enabled: false);
        }, fetchSuccess: (lat, lng) {
          return _buildPinDrop();
        }, fetchFailed: (error) {
          return _buildPinDrop();
        });
      },
    );
  }

  Widget _buildBottomSheet() {
    return DraggableScrollableSheet(
      initialChildSize: 0.15,
      minChildSize: 0.15,
      maxChildSize: maxSheetHeightRatio,
      builder: (BuildContext context, ScrollController scrollController) {
        return SingleChildScrollView(
          controller: scrollController,
          child: _buildInputs(),
        );
      },
    );
  }

  Widget _buildInputs() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      // height: MediaQuery.of(context).size.height * maxSheetHeightRatio,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
              width: 40,
              height: 8,
              margin: EdgeInsets.only(top: 8, bottom: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.black12,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Zoom Level - Target Location"),
            ),
            Slider(
              value: zoomLevel,
              min: 10,
              max: 18,
              onChanged: (v) {
                setState(() {
                  zoomLevel = v;
                });
                final center =
                    latLng.LatLng(targetGeofence.lat, targetGeofence.lng);
                mapController.move(center, this.zoomLevel);
              },
            ),
            _buildTextField(
              controller: wifiNameCtrl,
              labelText: 'Wifi Name',
              helperText: "Please enter Wifi name.",
              onChanged: (String v) {},
              inputType: TextInputType.text,
              onFieldSubmitted: (String v) {
                if (_formKey.currentState!.validate()) {
                  setState(() {
                    targetGeofence = targetGeofence.copyWith(wifiName: v);
                  });
                  BlocProvider.of<GeomapCubit>(context).updateTargetGeofence(
                      targetGeofence,
                      currentLocation: currentLocation);
                }
              },
              validator: (String? v) {
                if (v == null || v.isEmpty) {
                  return 'Please enter do not leave empty.';
                }
                return null;
              },
            ),
            _buildTextField(
              controller: latCtrl,
              labelText: 'Target location latitude',
              helperText: "range from -90.0 to 90.0",
              onChanged: (String v) {},
              inputType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
              onFieldSubmitted: (String v) {
                if (_formKey.currentState!.validate()) {
                  final _v = double.tryParse(v) ?? 0;
                  setState(() {
                    targetGeofence = targetGeofence.copyWith(lat: _v);
                  });
                  final center =
                      latLng.LatLng(targetGeofence.lat, targetGeofence.lng);
                  mapController.move(center, this.zoomLevel);
                  BlocProvider.of<GeomapCubit>(context).updateTargetGeofence(
                      targetGeofence,
                      currentLocation: currentLocation);
                }
              },
              validator: (String? v) {
                if (v == null || v.isEmpty) {
                  return 'Please enter do not leave empty.';
                }
                final _v = double.tryParse(v) ?? 0.0;
                if (_v < -90 || _v > 90) {
                  return 'Invalid lattitude value.';
                }
                return null;
              },
            ),
            _buildTextField(
              controller: lngCtrl,
              labelText: "Target location longitude",
              helperText: "range from -180.0 to 180.0",
              onChanged: (String v) {},
              inputType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
              onFieldSubmitted: (String v) {
                if (_formKey.currentState!.validate()) {
                  final _v = double.tryParse(v) ?? 0;
                  setState(() {
                    targetGeofence = targetGeofence.copyWith(lng: _v);
                  });
                  final center =
                      latLng.LatLng(targetGeofence.lat, targetGeofence.lng);
                  mapController.move(center, this.zoomLevel);
                  BlocProvider.of<GeomapCubit>(context).updateTargetGeofence(
                      targetGeofence,
                      currentLocation: currentLocation);
                }
              },
              validator: (String? v) {
                if (v == null || v.isEmpty) {
                  return 'Please enter do not leave empty.';
                }
                final _v = double.tryParse(v) ?? 0.0;
                if (_v < -180 || _v > 180) {
                  return 'Invalid longitude value.';
                }
                return null;
              },
            ),
            _buildTextField(
              controller: radiusCtrl,
              labelText: "Target location Radius",
              helperText: "1 = 1 meter",
              onChanged: (String v) {},
              inputType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
              onFieldSubmitted: (String v) {
                if (_formKey.currentState!.validate()) {
                  final _v = double.tryParse(v) ?? 0;
                  setState(() {
                    targetGeofence = targetGeofence.copyWith(radius: _v);
                  });
                  final center =
                      latLng.LatLng(targetGeofence.lat, targetGeofence.lng);
                  mapController.move(center, this.zoomLevel);
                  BlocProvider.of<GeomapCubit>(context).updateTargetGeofence(
                      targetGeofence,
                      currentLocation: currentLocation);
                }
              },
              validator: (String? v) {
                if (v == null || v.isEmpty) {
                  return 'Please enter do not leave empty.';
                }
                return null;
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextField({
    required TextEditingController controller,
    required String labelText,
    required String helperText,
    required Function(String v) onChanged,
    Function(String v)? onFieldSubmitted,
    required String? Function(String? v)? validator,
    required TextInputType inputType,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12),
      margin: EdgeInsets.only(bottom: 4),
      child: TextFormField(
        controller: controller,
        validator: validator,
        // controller: TextEditingController(text: null),
        keyboardType: inputType,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.zero,
          border: UnderlineInputBorder(),
          labelText: labelText,
          helperText: helperText,
        ),
        onChanged: onChanged,
        onFieldSubmitted: onFieldSubmitted ??
            (v) {
              _formKey.currentState!.validate();
            },
      ),
    );
  }
}
