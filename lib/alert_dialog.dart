import 'package:flutter/material.dart';

void showAlertDialog(BuildContext context, String title, String content) {
  showDialog(
    barrierColor: Colors.transparent,
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(16.0))),
        backgroundColor: Colors.black54,
        title: new Text(
          title,
          style: TextStyle(
            fontFamily: 'Lato',
            fontSize: 18,
            color: const Color(0xffffffff),
            fontWeight: FontWeight.w900,
            height: 0.5,
          ),
          textAlign: TextAlign.center,
        ),
        content: new Text(
          content,
          style: TextStyle(
            fontFamily: 'Lato',
            fontSize: 14,
            color: const Color(0xffffffff),
            fontWeight: FontWeight.w900,
            // height: 0.5,
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new TextButton(
            child: Text(
              'CLOSE',
              style: TextStyle(
                fontFamily: 'Lato',
                fontSize: 14,
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w900,
                height: 0.5,
              ),
              textAlign: TextAlign.center,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void showCustomDialog(
    BuildContext context, String title, String content, List<Widget> actions) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(title),
        content: new Text(content),
        actions: actions,
      );
    },
  );
}
