part of 'current_location_cubit.dart';

@freezed
abstract class CurrentLocationState with _$CurrentLocationState {
  const factory CurrentLocationState.initial() = _Initial;
  const factory CurrentLocationState.loading() = _Loading;
  const factory CurrentLocationState.fetchSuccess(
      double latitude, double longitude) = _FetchSuccess;
  const factory CurrentLocationState.fetchFailed(String error) = _FetchFailed;
}
