import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:location/location.dart';

part 'current_location_state.dart';
part 'current_location_cubit.freezed.dart';

class CurrentLocationCubit extends Cubit<CurrentLocationState> {
  CurrentLocationCubit({required this.geolocator})
      : super(CurrentLocationState.initial());

  final Location geolocator;

  void getCurrentPosition() async {
    emit(CurrentLocationState.loading());
    try {
      final position = await determinePosition();
      if (position.latitude == null || position.longitude == null) {
        emit(
            CurrentLocationState.fetchFailed("Invalid current location data."));
        return;
      }
      emit(CurrentLocationState.fetchSuccess(
          position.latitude!, position.longitude!));
    } catch (error) {
      emit(CurrentLocationState.fetchFailed(error.toString()));
    }
  }

  Future<LocationData> determinePosition() async {
    bool serviceEnabled;
    PermissionStatus permission;

    // Test if location services are enabled.
    serviceEnabled = await geolocator.serviceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      emit(CurrentLocationState.fetchFailed('Location services are disabled.'));
      return Future.error('Location services are disabled.');
    }

    permission = await geolocator.hasPermission();
    if (permission == PermissionStatus.denied) {
      permission = await geolocator.requestPermission();
      if (permission == PermissionStatus.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == PermissionStatus.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await geolocator.getLocation();
  }
}
