part of 'connectivity_cubit.dart';

@freezed
abstract class ConnectivityState with _$ConnectivityState {
  const factory ConnectivityState.initial() = _Initial;
  const factory ConnectivityState.connectedMobile() = _ConnectedMobile;
  const factory ConnectivityState.connectedWifi(String wifiName) =
      _ConnectedWifi;
  const factory ConnectivityState.disconnected(String? error) = _Disconnected;
}
