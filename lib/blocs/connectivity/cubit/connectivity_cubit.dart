import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/services.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:network_info_plus/network_info_plus.dart';

part 'connectivity_state.dart';
part 'connectivity_cubit.freezed.dart';

class ConnectivityCubit extends Cubit<ConnectivityState> {
  ConnectivityCubit({
    required this.connectivity,
    required this.networkInfo,
  }) : super(ConnectivityState.initial()) {
    subscription =
        this.connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  final Connectivity connectivity;
  final NetworkInfo networkInfo;
  late StreamSubscription subscription;

  @override
  Future<void> close() async {
    subscription.cancel();
    return super.close();
  }

  Future<void> initConnectivity() async {
    try {
      final result = await connectivity.checkConnectivity();
      _updateConnectionStatus(result);
    } on PlatformException catch (err) {
      emit(ConnectivityState.disconnected(err.toString()));
      return;
    }
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    if (result == ConnectivityResult.mobile) {
      emit(ConnectivityState.connectedMobile());
    } else if (result == ConnectivityResult.wifi) {
      if (Platform.isIOS) {
        var iosInfo = await DeviceInfoPlugin().iosInfo;
        if (!iosInfo.isPhysicalDevice) {
          emit(ConnectivityState.disconnected("Not suppported on Simulator"));
          return;
        }
      }
      // TODO: Read me add getWiFiName not working on emulator
      final wifiName = await networkInfo.getWifiName() ?? "Unidentified";
      emit(ConnectivityState.connectedWifi(wifiName));
    } else if (result == ConnectivityResult.none) {
      emit(ConnectivityState.disconnected(null));
    } else {
      print("Unknown status");
      emit(ConnectivityState.disconnected(null));
    }
  }
}
