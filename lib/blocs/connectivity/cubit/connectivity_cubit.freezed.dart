// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'connectivity_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ConnectivityStateTearOff {
  const _$ConnectivityStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _ConnectedMobile connectedMobile() {
    return const _ConnectedMobile();
  }

  _ConnectedWifi connectedWifi(String wifiName) {
    return _ConnectedWifi(
      wifiName,
    );
  }

  _Disconnected disconnected(String? error) {
    return _Disconnected(
      error,
    );
  }
}

/// @nodoc
const $ConnectivityState = _$ConnectivityStateTearOff();

/// @nodoc
mixin _$ConnectivityState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() connectedMobile,
    required TResult Function(String wifiName) connectedWifi,
    required TResult Function(String? error) disconnected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? connectedMobile,
    TResult Function(String wifiName)? connectedWifi,
    TResult Function(String? error)? disconnected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ConnectedMobile value) connectedMobile,
    required TResult Function(_ConnectedWifi value) connectedWifi,
    required TResult Function(_Disconnected value) disconnected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ConnectedMobile value)? connectedMobile,
    TResult Function(_ConnectedWifi value)? connectedWifi,
    TResult Function(_Disconnected value)? disconnected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ConnectivityStateCopyWith<$Res> {
  factory $ConnectivityStateCopyWith(
          ConnectivityState value, $Res Function(ConnectivityState) then) =
      _$ConnectivityStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ConnectivityStateCopyWithImpl<$Res>
    implements $ConnectivityStateCopyWith<$Res> {
  _$ConnectivityStateCopyWithImpl(this._value, this._then);

  final ConnectivityState _value;
  // ignore: unused_field
  final $Res Function(ConnectivityState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$ConnectivityStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'ConnectivityState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() connectedMobile,
    required TResult Function(String wifiName) connectedWifi,
    required TResult Function(String? error) disconnected,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? connectedMobile,
    TResult Function(String wifiName)? connectedWifi,
    TResult Function(String? error)? disconnected,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ConnectedMobile value) connectedMobile,
    required TResult Function(_ConnectedWifi value) connectedWifi,
    required TResult Function(_Disconnected value) disconnected,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ConnectedMobile value)? connectedMobile,
    TResult Function(_ConnectedWifi value)? connectedWifi,
    TResult Function(_Disconnected value)? disconnected,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ConnectivityState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$ConnectedMobileCopyWith<$Res> {
  factory _$ConnectedMobileCopyWith(
          _ConnectedMobile value, $Res Function(_ConnectedMobile) then) =
      __$ConnectedMobileCopyWithImpl<$Res>;
}

/// @nodoc
class __$ConnectedMobileCopyWithImpl<$Res>
    extends _$ConnectivityStateCopyWithImpl<$Res>
    implements _$ConnectedMobileCopyWith<$Res> {
  __$ConnectedMobileCopyWithImpl(
      _ConnectedMobile _value, $Res Function(_ConnectedMobile) _then)
      : super(_value, (v) => _then(v as _ConnectedMobile));

  @override
  _ConnectedMobile get _value => super._value as _ConnectedMobile;
}

/// @nodoc

class _$_ConnectedMobile implements _ConnectedMobile {
  const _$_ConnectedMobile();

  @override
  String toString() {
    return 'ConnectivityState.connectedMobile()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _ConnectedMobile);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() connectedMobile,
    required TResult Function(String wifiName) connectedWifi,
    required TResult Function(String? error) disconnected,
  }) {
    return connectedMobile();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? connectedMobile,
    TResult Function(String wifiName)? connectedWifi,
    TResult Function(String? error)? disconnected,
    required TResult orElse(),
  }) {
    if (connectedMobile != null) {
      return connectedMobile();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ConnectedMobile value) connectedMobile,
    required TResult Function(_ConnectedWifi value) connectedWifi,
    required TResult Function(_Disconnected value) disconnected,
  }) {
    return connectedMobile(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ConnectedMobile value)? connectedMobile,
    TResult Function(_ConnectedWifi value)? connectedWifi,
    TResult Function(_Disconnected value)? disconnected,
    required TResult orElse(),
  }) {
    if (connectedMobile != null) {
      return connectedMobile(this);
    }
    return orElse();
  }
}

abstract class _ConnectedMobile implements ConnectivityState {
  const factory _ConnectedMobile() = _$_ConnectedMobile;
}

/// @nodoc
abstract class _$ConnectedWifiCopyWith<$Res> {
  factory _$ConnectedWifiCopyWith(
          _ConnectedWifi value, $Res Function(_ConnectedWifi) then) =
      __$ConnectedWifiCopyWithImpl<$Res>;
  $Res call({String wifiName});
}

/// @nodoc
class __$ConnectedWifiCopyWithImpl<$Res>
    extends _$ConnectivityStateCopyWithImpl<$Res>
    implements _$ConnectedWifiCopyWith<$Res> {
  __$ConnectedWifiCopyWithImpl(
      _ConnectedWifi _value, $Res Function(_ConnectedWifi) _then)
      : super(_value, (v) => _then(v as _ConnectedWifi));

  @override
  _ConnectedWifi get _value => super._value as _ConnectedWifi;

  @override
  $Res call({
    Object? wifiName = freezed,
  }) {
    return _then(_ConnectedWifi(
      wifiName == freezed
          ? _value.wifiName
          : wifiName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ConnectedWifi implements _ConnectedWifi {
  const _$_ConnectedWifi(this.wifiName);

  @override
  final String wifiName;

  @override
  String toString() {
    return 'ConnectivityState.connectedWifi(wifiName: $wifiName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ConnectedWifi &&
            (identical(other.wifiName, wifiName) ||
                const DeepCollectionEquality()
                    .equals(other.wifiName, wifiName)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(wifiName);

  @JsonKey(ignore: true)
  @override
  _$ConnectedWifiCopyWith<_ConnectedWifi> get copyWith =>
      __$ConnectedWifiCopyWithImpl<_ConnectedWifi>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() connectedMobile,
    required TResult Function(String wifiName) connectedWifi,
    required TResult Function(String? error) disconnected,
  }) {
    return connectedWifi(wifiName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? connectedMobile,
    TResult Function(String wifiName)? connectedWifi,
    TResult Function(String? error)? disconnected,
    required TResult orElse(),
  }) {
    if (connectedWifi != null) {
      return connectedWifi(wifiName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ConnectedMobile value) connectedMobile,
    required TResult Function(_ConnectedWifi value) connectedWifi,
    required TResult Function(_Disconnected value) disconnected,
  }) {
    return connectedWifi(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ConnectedMobile value)? connectedMobile,
    TResult Function(_ConnectedWifi value)? connectedWifi,
    TResult Function(_Disconnected value)? disconnected,
    required TResult orElse(),
  }) {
    if (connectedWifi != null) {
      return connectedWifi(this);
    }
    return orElse();
  }
}

abstract class _ConnectedWifi implements ConnectivityState {
  const factory _ConnectedWifi(String wifiName) = _$_ConnectedWifi;

  String get wifiName => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$ConnectedWifiCopyWith<_ConnectedWifi> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$DisconnectedCopyWith<$Res> {
  factory _$DisconnectedCopyWith(
          _Disconnected value, $Res Function(_Disconnected) then) =
      __$DisconnectedCopyWithImpl<$Res>;
  $Res call({String? error});
}

/// @nodoc
class __$DisconnectedCopyWithImpl<$Res>
    extends _$ConnectivityStateCopyWithImpl<$Res>
    implements _$DisconnectedCopyWith<$Res> {
  __$DisconnectedCopyWithImpl(
      _Disconnected _value, $Res Function(_Disconnected) _then)
      : super(_value, (v) => _then(v as _Disconnected));

  @override
  _Disconnected get _value => super._value as _Disconnected;

  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_Disconnected(
      error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_Disconnected implements _Disconnected {
  const _$_Disconnected(this.error);

  @override
  final String? error;

  @override
  String toString() {
    return 'ConnectivityState.disconnected(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Disconnected &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @JsonKey(ignore: true)
  @override
  _$DisconnectedCopyWith<_Disconnected> get copyWith =>
      __$DisconnectedCopyWithImpl<_Disconnected>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() connectedMobile,
    required TResult Function(String wifiName) connectedWifi,
    required TResult Function(String? error) disconnected,
  }) {
    return disconnected(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? connectedMobile,
    TResult Function(String wifiName)? connectedWifi,
    TResult Function(String? error)? disconnected,
    required TResult orElse(),
  }) {
    if (disconnected != null) {
      return disconnected(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ConnectedMobile value) connectedMobile,
    required TResult Function(_ConnectedWifi value) connectedWifi,
    required TResult Function(_Disconnected value) disconnected,
  }) {
    return disconnected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ConnectedMobile value)? connectedMobile,
    TResult Function(_ConnectedWifi value)? connectedWifi,
    TResult Function(_Disconnected value)? disconnected,
    required TResult orElse(),
  }) {
    if (disconnected != null) {
      return disconnected(this);
    }
    return orElse();
  }
}

abstract class _Disconnected implements ConnectivityState {
  const factory _Disconnected(String? error) = _$_Disconnected;

  String? get error => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$DisconnectedCopyWith<_Disconnected> get copyWith =>
      throw _privateConstructorUsedError;
}
