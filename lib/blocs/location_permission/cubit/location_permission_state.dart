part of 'location_permission_cubit.dart';

@freezed
abstract class LocationPermissionState with _$LocationPermissionState {
  const factory LocationPermissionState.initial() = _Initial;
  const factory LocationPermissionState.permissionGranted() =
      _PermissionGranted;
  const factory LocationPermissionState.permissionDenied() = _PermissionDenied;
}
