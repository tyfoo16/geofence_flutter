// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'location_permission_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$LocationPermissionStateTearOff {
  const _$LocationPermissionStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _PermissionGranted permissionGranted() {
    return const _PermissionGranted();
  }

  _PermissionDenied permissionDenied() {
    return const _PermissionDenied();
  }
}

/// @nodoc
const $LocationPermissionState = _$LocationPermissionStateTearOff();

/// @nodoc
mixin _$LocationPermissionState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() permissionGranted,
    required TResult Function() permissionDenied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? permissionGranted,
    TResult Function()? permissionDenied,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_PermissionGranted value) permissionGranted,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_PermissionGranted value)? permissionGranted,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocationPermissionStateCopyWith<$Res> {
  factory $LocationPermissionStateCopyWith(LocationPermissionState value,
          $Res Function(LocationPermissionState) then) =
      _$LocationPermissionStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$LocationPermissionStateCopyWithImpl<$Res>
    implements $LocationPermissionStateCopyWith<$Res> {
  _$LocationPermissionStateCopyWithImpl(this._value, this._then);

  final LocationPermissionState _value;
  // ignore: unused_field
  final $Res Function(LocationPermissionState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$LocationPermissionStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'LocationPermissionState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() permissionGranted,
    required TResult Function() permissionDenied,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? permissionGranted,
    TResult Function()? permissionDenied,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_PermissionGranted value) permissionGranted,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_PermissionGranted value)? permissionGranted,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements LocationPermissionState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$PermissionGrantedCopyWith<$Res> {
  factory _$PermissionGrantedCopyWith(
          _PermissionGranted value, $Res Function(_PermissionGranted) then) =
      __$PermissionGrantedCopyWithImpl<$Res>;
}

/// @nodoc
class __$PermissionGrantedCopyWithImpl<$Res>
    extends _$LocationPermissionStateCopyWithImpl<$Res>
    implements _$PermissionGrantedCopyWith<$Res> {
  __$PermissionGrantedCopyWithImpl(
      _PermissionGranted _value, $Res Function(_PermissionGranted) _then)
      : super(_value, (v) => _then(v as _PermissionGranted));

  @override
  _PermissionGranted get _value => super._value as _PermissionGranted;
}

/// @nodoc

class _$_PermissionGranted implements _PermissionGranted {
  const _$_PermissionGranted();

  @override
  String toString() {
    return 'LocationPermissionState.permissionGranted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _PermissionGranted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() permissionGranted,
    required TResult Function() permissionDenied,
  }) {
    return permissionGranted();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? permissionGranted,
    TResult Function()? permissionDenied,
    required TResult orElse(),
  }) {
    if (permissionGranted != null) {
      return permissionGranted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_PermissionGranted value) permissionGranted,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return permissionGranted(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_PermissionGranted value)? permissionGranted,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (permissionGranted != null) {
      return permissionGranted(this);
    }
    return orElse();
  }
}

abstract class _PermissionGranted implements LocationPermissionState {
  const factory _PermissionGranted() = _$_PermissionGranted;
}

/// @nodoc
abstract class _$PermissionDeniedCopyWith<$Res> {
  factory _$PermissionDeniedCopyWith(
          _PermissionDenied value, $Res Function(_PermissionDenied) then) =
      __$PermissionDeniedCopyWithImpl<$Res>;
}

/// @nodoc
class __$PermissionDeniedCopyWithImpl<$Res>
    extends _$LocationPermissionStateCopyWithImpl<$Res>
    implements _$PermissionDeniedCopyWith<$Res> {
  __$PermissionDeniedCopyWithImpl(
      _PermissionDenied _value, $Res Function(_PermissionDenied) _then)
      : super(_value, (v) => _then(v as _PermissionDenied));

  @override
  _PermissionDenied get _value => super._value as _PermissionDenied;
}

/// @nodoc

class _$_PermissionDenied implements _PermissionDenied {
  const _$_PermissionDenied();

  @override
  String toString() {
    return 'LocationPermissionState.permissionDenied()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _PermissionDenied);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() permissionGranted,
    required TResult Function() permissionDenied,
  }) {
    return permissionDenied();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? permissionGranted,
    TResult Function()? permissionDenied,
    required TResult orElse(),
  }) {
    if (permissionDenied != null) {
      return permissionDenied();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_PermissionGranted value) permissionGranted,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return permissionDenied(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_PermissionGranted value)? permissionGranted,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (permissionDenied != null) {
      return permissionDenied(this);
    }
    return orElse();
  }
}

abstract class _PermissionDenied implements LocationPermissionState {
  const factory _PermissionDenied() = _$_PermissionDenied;
}
