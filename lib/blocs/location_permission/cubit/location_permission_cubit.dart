import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';

part 'location_permission_state.dart';
part 'location_permission_cubit.freezed.dart';

class LocationPermissionCubit extends Cubit<LocationPermissionState> {
  LocationPermissionCubit(this.networkInfo)
      : super(LocationPermissionState.initial());
  final NetworkInfo networkInfo;

  Future<void> requestPermission() async {
    try {
      if (Platform.isIOS) {
        var status = await networkInfo.getLocationServiceAuthorization();
        if (status == LocationAuthorizationStatus.notDetermined) {
          emit(LocationPermissionState.permissionDenied());
          status = await networkInfo.requestLocationServiceAuthorization();
        }
        if (status == LocationAuthorizationStatus.authorizedAlways ||
            status == LocationAuthorizationStatus.authorizedWhenInUse) {
          emit(LocationPermissionState.permissionGranted());
        } else {
          emit(LocationPermissionState.permissionDenied());
          await openAppSettings();
        }
      } else if (Platform.isAndroid) {
        Map<Permission, PermissionStatus> statuses =
            await [Permission.locationWhenInUse].request();
        PermissionStatus? locationStatus =
            statuses[Permission.locationWhenInUse];
        if (locationStatus == PermissionStatus.granted) {
          emit(LocationPermissionState.permissionGranted());
          // initConnectivity();
          return;
        } else {
          emit(LocationPermissionState.permissionDenied());
          final result = await [Permission.locationWhenInUse].request();
          final status = result[Permission.locationWhenInUse];
          if (status == PermissionStatus.denied ||
              status == PermissionStatus.permanentlyDenied ||
              status == PermissionStatus.limited ||
              status == PermissionStatus.restricted) {
            await openAppSettings();
          }

          return;
        }
      }
    } catch (err) {
      // emit(ConnectivityState.disconnected(err.toString()));
    }
  }
}
