part of 'geomap_cubit.dart';

@freezed
abstract class GeomapState with _$GeomapState {
  const factory GeomapState.initial() = _Initial;
  const factory GeomapState.targetUpdated(
      Geofence geofence, bool withInRadius) = _TargetUpdated;
  const factory GeomapState.networkUpdated(
      NetworkInfo info, bool permissionGranted) = _NetworkUpdated;
  const factory GeomapState.currentLocationUpdated(double lat, double lng) =
      _CurrentLocationUpdated;
}
