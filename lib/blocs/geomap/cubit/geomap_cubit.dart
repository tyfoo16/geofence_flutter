import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:geofence_flutter/blocs/current_location/cubit/current_location_cubit.dart';
import 'package:geofence_flutter/models/geofence.dart';
import 'package:geolocator/geolocator.dart';
import "package:latlong2/latlong.dart" as latLng;
import 'package:network_info_plus/network_info_plus.dart';

part 'geomap_state.dart';
part 'geomap_cubit.freezed.dart';

class GeomapCubit extends Cubit<GeomapState> {
  final CurrentLocationCubit currentLocationBloc;
  late StreamSubscription currentLocationSub;
  latLng.LatLng? _currentLocation;
  Geofence? targetGeofence;

  GeomapCubit(this.currentLocationBloc) : super(GeomapState.initial()) {
    currentLocationSub = currentLocationBloc.stream.listen((state) {
      state.when(
          initial: () {},
          loading: () {
            _currentLocation = null;
          },
          fetchSuccess: (lat, lng) {
            _currentLocation = latLng.LatLng(lat, lng);
          },
          fetchFailed: (err) {
            _currentLocation = null;
          });
    });
  }

  @override
  Future<void> close() {
    currentLocationSub.cancel();
    return super.close();
  }

  void updateTargetGeofence(Geofence updated,
      {latLng.LatLng? currentLocation}) {
    _currentLocation = currentLocation;
    targetGeofence = updated;

    if (targetGeofence != null && _currentLocation != null) {
      final targetPos = latLng.LatLng(targetGeofence!.lat, targetGeofence!.lng);
      final distance = Geolocator.distanceBetween(
          targetPos.latitude,
          targetPos.longitude,
          currentLocation!.latitude,
          currentLocation.longitude);

      final withInRadius = distance <= targetGeofence!.radius;

      emit(GeomapState.targetUpdated(updated, withInRadius));
    } else {
      emit(GeomapState.targetUpdated(updated, false));
    }
  }
}
