// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'geomap_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GeomapStateTearOff {
  const _$GeomapStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _TargetUpdated targetUpdated(Geofence geofence, bool withInRadius) {
    return _TargetUpdated(
      geofence,
      withInRadius,
    );
  }

  _NetworkUpdated networkUpdated(NetworkInfo info, bool permissionGranted) {
    return _NetworkUpdated(
      info,
      permissionGranted,
    );
  }

  _CurrentLocationUpdated currentLocationUpdated(double lat, double lng) {
    return _CurrentLocationUpdated(
      lat,
      lng,
    );
  }
}

/// @nodoc
const $GeomapState = _$GeomapStateTearOff();

/// @nodoc
mixin _$GeomapState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Geofence geofence, bool withInRadius)
        targetUpdated,
    required TResult Function(NetworkInfo info, bool permissionGranted)
        networkUpdated,
    required TResult Function(double lat, double lng) currentLocationUpdated,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Geofence geofence, bool withInRadius)? targetUpdated,
    TResult Function(NetworkInfo info, bool permissionGranted)? networkUpdated,
    TResult Function(double lat, double lng)? currentLocationUpdated,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_TargetUpdated value) targetUpdated,
    required TResult Function(_NetworkUpdated value) networkUpdated,
    required TResult Function(_CurrentLocationUpdated value)
        currentLocationUpdated,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_TargetUpdated value)? targetUpdated,
    TResult Function(_NetworkUpdated value)? networkUpdated,
    TResult Function(_CurrentLocationUpdated value)? currentLocationUpdated,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GeomapStateCopyWith<$Res> {
  factory $GeomapStateCopyWith(
          GeomapState value, $Res Function(GeomapState) then) =
      _$GeomapStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$GeomapStateCopyWithImpl<$Res> implements $GeomapStateCopyWith<$Res> {
  _$GeomapStateCopyWithImpl(this._value, this._then);

  final GeomapState _value;
  // ignore: unused_field
  final $Res Function(GeomapState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$GeomapStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'GeomapState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Geofence geofence, bool withInRadius)
        targetUpdated,
    required TResult Function(NetworkInfo info, bool permissionGranted)
        networkUpdated,
    required TResult Function(double lat, double lng) currentLocationUpdated,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Geofence geofence, bool withInRadius)? targetUpdated,
    TResult Function(NetworkInfo info, bool permissionGranted)? networkUpdated,
    TResult Function(double lat, double lng)? currentLocationUpdated,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_TargetUpdated value) targetUpdated,
    required TResult Function(_NetworkUpdated value) networkUpdated,
    required TResult Function(_CurrentLocationUpdated value)
        currentLocationUpdated,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_TargetUpdated value)? targetUpdated,
    TResult Function(_NetworkUpdated value)? networkUpdated,
    TResult Function(_CurrentLocationUpdated value)? currentLocationUpdated,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements GeomapState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$TargetUpdatedCopyWith<$Res> {
  factory _$TargetUpdatedCopyWith(
          _TargetUpdated value, $Res Function(_TargetUpdated) then) =
      __$TargetUpdatedCopyWithImpl<$Res>;
  $Res call({Geofence geofence, bool withInRadius});

  $GeofenceCopyWith<$Res> get geofence;
}

/// @nodoc
class __$TargetUpdatedCopyWithImpl<$Res> extends _$GeomapStateCopyWithImpl<$Res>
    implements _$TargetUpdatedCopyWith<$Res> {
  __$TargetUpdatedCopyWithImpl(
      _TargetUpdated _value, $Res Function(_TargetUpdated) _then)
      : super(_value, (v) => _then(v as _TargetUpdated));

  @override
  _TargetUpdated get _value => super._value as _TargetUpdated;

  @override
  $Res call({
    Object? geofence = freezed,
    Object? withInRadius = freezed,
  }) {
    return _then(_TargetUpdated(
      geofence == freezed
          ? _value.geofence
          : geofence // ignore: cast_nullable_to_non_nullable
              as Geofence,
      withInRadius == freezed
          ? _value.withInRadius
          : withInRadius // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }

  @override
  $GeofenceCopyWith<$Res> get geofence {
    return $GeofenceCopyWith<$Res>(_value.geofence, (value) {
      return _then(_value.copyWith(geofence: value));
    });
  }
}

/// @nodoc

class _$_TargetUpdated implements _TargetUpdated {
  const _$_TargetUpdated(this.geofence, this.withInRadius);

  @override
  final Geofence geofence;
  @override
  final bool withInRadius;

  @override
  String toString() {
    return 'GeomapState.targetUpdated(geofence: $geofence, withInRadius: $withInRadius)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TargetUpdated &&
            (identical(other.geofence, geofence) ||
                const DeepCollectionEquality()
                    .equals(other.geofence, geofence)) &&
            (identical(other.withInRadius, withInRadius) ||
                const DeepCollectionEquality()
                    .equals(other.withInRadius, withInRadius)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(geofence) ^
      const DeepCollectionEquality().hash(withInRadius);

  @JsonKey(ignore: true)
  @override
  _$TargetUpdatedCopyWith<_TargetUpdated> get copyWith =>
      __$TargetUpdatedCopyWithImpl<_TargetUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Geofence geofence, bool withInRadius)
        targetUpdated,
    required TResult Function(NetworkInfo info, bool permissionGranted)
        networkUpdated,
    required TResult Function(double lat, double lng) currentLocationUpdated,
  }) {
    return targetUpdated(geofence, withInRadius);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Geofence geofence, bool withInRadius)? targetUpdated,
    TResult Function(NetworkInfo info, bool permissionGranted)? networkUpdated,
    TResult Function(double lat, double lng)? currentLocationUpdated,
    required TResult orElse(),
  }) {
    if (targetUpdated != null) {
      return targetUpdated(geofence, withInRadius);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_TargetUpdated value) targetUpdated,
    required TResult Function(_NetworkUpdated value) networkUpdated,
    required TResult Function(_CurrentLocationUpdated value)
        currentLocationUpdated,
  }) {
    return targetUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_TargetUpdated value)? targetUpdated,
    TResult Function(_NetworkUpdated value)? networkUpdated,
    TResult Function(_CurrentLocationUpdated value)? currentLocationUpdated,
    required TResult orElse(),
  }) {
    if (targetUpdated != null) {
      return targetUpdated(this);
    }
    return orElse();
  }
}

abstract class _TargetUpdated implements GeomapState {
  const factory _TargetUpdated(Geofence geofence, bool withInRadius) =
      _$_TargetUpdated;

  Geofence get geofence => throw _privateConstructorUsedError;
  bool get withInRadius => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$TargetUpdatedCopyWith<_TargetUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$NetworkUpdatedCopyWith<$Res> {
  factory _$NetworkUpdatedCopyWith(
          _NetworkUpdated value, $Res Function(_NetworkUpdated) then) =
      __$NetworkUpdatedCopyWithImpl<$Res>;
  $Res call({NetworkInfo info, bool permissionGranted});
}

/// @nodoc
class __$NetworkUpdatedCopyWithImpl<$Res>
    extends _$GeomapStateCopyWithImpl<$Res>
    implements _$NetworkUpdatedCopyWith<$Res> {
  __$NetworkUpdatedCopyWithImpl(
      _NetworkUpdated _value, $Res Function(_NetworkUpdated) _then)
      : super(_value, (v) => _then(v as _NetworkUpdated));

  @override
  _NetworkUpdated get _value => super._value as _NetworkUpdated;

  @override
  $Res call({
    Object? info = freezed,
    Object? permissionGranted = freezed,
  }) {
    return _then(_NetworkUpdated(
      info == freezed
          ? _value.info
          : info // ignore: cast_nullable_to_non_nullable
              as NetworkInfo,
      permissionGranted == freezed
          ? _value.permissionGranted
          : permissionGranted // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_NetworkUpdated implements _NetworkUpdated {
  const _$_NetworkUpdated(this.info, this.permissionGranted);

  @override
  final NetworkInfo info;
  @override
  final bool permissionGranted;

  @override
  String toString() {
    return 'GeomapState.networkUpdated(info: $info, permissionGranted: $permissionGranted)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NetworkUpdated &&
            (identical(other.info, info) ||
                const DeepCollectionEquality().equals(other.info, info)) &&
            (identical(other.permissionGranted, permissionGranted) ||
                const DeepCollectionEquality()
                    .equals(other.permissionGranted, permissionGranted)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(info) ^
      const DeepCollectionEquality().hash(permissionGranted);

  @JsonKey(ignore: true)
  @override
  _$NetworkUpdatedCopyWith<_NetworkUpdated> get copyWith =>
      __$NetworkUpdatedCopyWithImpl<_NetworkUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Geofence geofence, bool withInRadius)
        targetUpdated,
    required TResult Function(NetworkInfo info, bool permissionGranted)
        networkUpdated,
    required TResult Function(double lat, double lng) currentLocationUpdated,
  }) {
    return networkUpdated(info, permissionGranted);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Geofence geofence, bool withInRadius)? targetUpdated,
    TResult Function(NetworkInfo info, bool permissionGranted)? networkUpdated,
    TResult Function(double lat, double lng)? currentLocationUpdated,
    required TResult orElse(),
  }) {
    if (networkUpdated != null) {
      return networkUpdated(info, permissionGranted);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_TargetUpdated value) targetUpdated,
    required TResult Function(_NetworkUpdated value) networkUpdated,
    required TResult Function(_CurrentLocationUpdated value)
        currentLocationUpdated,
  }) {
    return networkUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_TargetUpdated value)? targetUpdated,
    TResult Function(_NetworkUpdated value)? networkUpdated,
    TResult Function(_CurrentLocationUpdated value)? currentLocationUpdated,
    required TResult orElse(),
  }) {
    if (networkUpdated != null) {
      return networkUpdated(this);
    }
    return orElse();
  }
}

abstract class _NetworkUpdated implements GeomapState {
  const factory _NetworkUpdated(NetworkInfo info, bool permissionGranted) =
      _$_NetworkUpdated;

  NetworkInfo get info => throw _privateConstructorUsedError;
  bool get permissionGranted => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$NetworkUpdatedCopyWith<_NetworkUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$CurrentLocationUpdatedCopyWith<$Res> {
  factory _$CurrentLocationUpdatedCopyWith(_CurrentLocationUpdated value,
          $Res Function(_CurrentLocationUpdated) then) =
      __$CurrentLocationUpdatedCopyWithImpl<$Res>;
  $Res call({double lat, double lng});
}

/// @nodoc
class __$CurrentLocationUpdatedCopyWithImpl<$Res>
    extends _$GeomapStateCopyWithImpl<$Res>
    implements _$CurrentLocationUpdatedCopyWith<$Res> {
  __$CurrentLocationUpdatedCopyWithImpl(_CurrentLocationUpdated _value,
      $Res Function(_CurrentLocationUpdated) _then)
      : super(_value, (v) => _then(v as _CurrentLocationUpdated));

  @override
  _CurrentLocationUpdated get _value => super._value as _CurrentLocationUpdated;

  @override
  $Res call({
    Object? lat = freezed,
    Object? lng = freezed,
  }) {
    return _then(_CurrentLocationUpdated(
      lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double,
      lng == freezed
          ? _value.lng
          : lng // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$_CurrentLocationUpdated implements _CurrentLocationUpdated {
  const _$_CurrentLocationUpdated(this.lat, this.lng);

  @override
  final double lat;
  @override
  final double lng;

  @override
  String toString() {
    return 'GeomapState.currentLocationUpdated(lat: $lat, lng: $lng)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CurrentLocationUpdated &&
            (identical(other.lat, lat) ||
                const DeepCollectionEquality().equals(other.lat, lat)) &&
            (identical(other.lng, lng) ||
                const DeepCollectionEquality().equals(other.lng, lng)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(lat) ^
      const DeepCollectionEquality().hash(lng);

  @JsonKey(ignore: true)
  @override
  _$CurrentLocationUpdatedCopyWith<_CurrentLocationUpdated> get copyWith =>
      __$CurrentLocationUpdatedCopyWithImpl<_CurrentLocationUpdated>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Geofence geofence, bool withInRadius)
        targetUpdated,
    required TResult Function(NetworkInfo info, bool permissionGranted)
        networkUpdated,
    required TResult Function(double lat, double lng) currentLocationUpdated,
  }) {
    return currentLocationUpdated(lat, lng);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Geofence geofence, bool withInRadius)? targetUpdated,
    TResult Function(NetworkInfo info, bool permissionGranted)? networkUpdated,
    TResult Function(double lat, double lng)? currentLocationUpdated,
    required TResult orElse(),
  }) {
    if (currentLocationUpdated != null) {
      return currentLocationUpdated(lat, lng);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_TargetUpdated value) targetUpdated,
    required TResult Function(_NetworkUpdated value) networkUpdated,
    required TResult Function(_CurrentLocationUpdated value)
        currentLocationUpdated,
  }) {
    return currentLocationUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_TargetUpdated value)? targetUpdated,
    TResult Function(_NetworkUpdated value)? networkUpdated,
    TResult Function(_CurrentLocationUpdated value)? currentLocationUpdated,
    required TResult orElse(),
  }) {
    if (currentLocationUpdated != null) {
      return currentLocationUpdated(this);
    }
    return orElse();
  }
}

abstract class _CurrentLocationUpdated implements GeomapState {
  const factory _CurrentLocationUpdated(double lat, double lng) =
      _$_CurrentLocationUpdated;

  double get lat => throw _privateConstructorUsedError;
  double get lng => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$CurrentLocationUpdatedCopyWith<_CurrentLocationUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}
