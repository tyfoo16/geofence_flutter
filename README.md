# geofence_flutter

# Getting Started


![](https://media.giphy.com/media/DmJMnUhmF3xRAQ5KzW/giphy.gif)


## **Environment**

* Flutter 2.5.0

## **Supported Platform**
* Android - Yes
* iOS - Yes 

## **Setup**
* run in terminal - flutter pub get
* set initial target configuration in lib/config.dart

## **UI**
### *Drop Pins*
* Green : Your device location
* Blue : Target location

### *Circle marker*
* represent target location coverage area. Can be update through input in Radius text field.

### *Inputs*
* Draggable bottom sheet form 
    * Zoom Level : Edit the camera zoom level with center on target location.
    * Wifi Name:  wifi name to match with connected network name(Wifi).
    * Target Location Lattitude
    * Target Location Longitude
    * Target Location Radius
* Data will be updated on text field submitted (Keyboard tick/check button)
* Current Location button - at bottom right
* Geofence Status window - at top
    * Network type: Wifi, Mobile Data
    * Wifi name: Connected Wifi network name
    * Wifi name Matched: checking on input wifi name and connected wifi name
    * Within radius: check if device location within the target radius.
    * Status: check if the device is connected to the specified WiFi network or remains geographically inside the circle.

## **Notes**
* Location permission is required.
* Get wifi name is not working in emulator. Please run in real device.